// Update the following values
var popup = null;

function openPopUp(url) {
    var width = 525, height = 525, top, left;

    if (window['isIE']) {
        var screenLeft = window.screenLeft,
                screenTop = window.screenTop,
                docElement = document.documentElement,
                titleHeight = 30;

        left = screenLeft + (docElement.clientWidth - width) / 2;
        top = screenTop + (docElement.clientHeight - height) / 2 - titleHeight;
    }
    else {
        var screenX = window.screenX,
                screenY = window.screenY,
                outerWidth = window.outerWidth,
                outerHeight = window.outerHeight;

        left = screenX + (outerWidth - width) / 2;
        top = screenY + (outerHeight - height) / 2;
    }

    var features = [
        "width=" + width,
        "height=" + height,
        "top=" + top,
        "left=" + left,
        "status=no",
        "resizable=yes",
        "toolbar=no",
        "menubar=no",
        "scrollbars=yes"];

    var popup = window.open(url, "oauth", features.join(","));
    popup.focus();

    return popup;
}

function Login(auth_url)
{
    popup = openPopUp(auth_url);
}

function getCookie(name) {
    var cookies = document.cookie;

    // Look for 'name='
    name += "=";

    var start = cookies.indexOf(name);
    if (start >= 0) {
        start += name.length;

        var end = cookies.indexOf(';', start);
        if (end < 0) {
            end = cookies.length;
        }
        var value = cookies.substring(start, end);
        return value;
    }
    return "";
}

function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

function checkLogin() {
//    for now: only happen on the homepage
    if (document.getElementsByTagName("title")[0].textContent === "Notifyer") {
      document.getElementById("login_btn").disabled = (docCookies.getItem("access_token") != "");
    }
}

function toggleButtons() {
//    for now: only happen on the homepage
	if (document.getElementsByTagName("title")[0].textContent === "Notifyer") {
      var access_token = docCookies.hasItem("access_token"), pb_access_token = docCookies.hasItem("pb_access_token");
      if(access_token) {
        document.getElementById("onenote_auth").setAttribute("data-is-visible", false);
        document.getElementById("onenote_revoke").setAttribute("data-is-visible", true);
      }
      else {
        document.getElementById("onenote_auth").setAttribute("data-is-visible", true);
        document.getElementById("onenote_revoke").setAttribute("data-is-visible", false);
      }
      
      if(pb_access_token) {
        document.getElementById("pb_auth").setAttribute("data-is-visible", false);
        document.getElementById("pb_revoke").setAttribute("data-is-visible", true);
      }
      else {
        document.getElementById("pb_auth").setAttribute("data-is-visible", true);
        document.getElementById("pb_revoke").setAttribute("data-is-visible", false);
      }
      
      if(access_token && pb_access_token) {
        document.getElementById("view-notes").setAttribute("data-is-disabled", false);
      }
      else {
        document.getElementById("view-notes").setAttribute("data-is-disabled", true);
      }
      
      
//      if (document.getElementById("view-notes").getAttribute("disabled") === "disabled" && (docCookies.getItem("access_token") != "")) {
//          location.reload(true);
//      }
    }
}

function toggleAttribute(elem, attr) {
  elem.setAttribute(attr, !elem.getAttribute(attr));
}

//window.setInterval (checkLogin, 1000);
window.setInterval (toggleButtons, 1000);
//    deleteAllCookies();