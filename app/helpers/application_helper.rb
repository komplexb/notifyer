require 'pushbullet_api' # see lib/pushbullet.rb

module ApplicationHelper
  def pb_access_token
    pb_access_token = cookies['pb_access_token']
#    session['pb_access_token'].nil? ? '' : session['pb_access_token'][:value]
  end

  def pushbullet_auth_url
    pushbullet = PushbulletAPI.new
    pushbullet_auth_url = pushbullet.auth_url
  end

  def access_token
    access_token = cookies['access_token']
  end

  def auth_url
    onenote_client = OneNoteSharer.new
    auth_url = onenote_client.auth_url
  end
  
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Notifyer"
    
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end
