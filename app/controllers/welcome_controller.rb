require 'one_note_sharer' # see lib/one_note_sharer.rb
require 'pushbullet_api' # see lib/pushbullet.rb

include ApplicationHelper

class WelcomeController < ApplicationController
  @resourse_url

  def index
    # OneNote
    @auth_url = auth_url
    @has_access_token = !access_token.blank?
    @onenote_revoke_url = ONENOTE_CONFIG['revoke']
    
    # Pushbullet
    @pushbullet_auth_url = pushbullet_auth_url
    @has_pb_access_token = !pb_access_token.blank?
#    debugger
    @pushbullet_revoke_url = PUSHBULLET_CONFIG['revoke']
  end
  
  def callback
    onenote_client = OneNoteSharer.new
    token_set = onenote_client.handle_callback_request(params)
    if token_set.present?
      expire_in = token_set['expires_in'].to_i
      cookies['access_token'] = { :value => token_set['access_token'], :expires => expire_in.seconds.from_now }
      cookies['authentication_token'] = { :value => token_set['authentication_token'], :expires => expire_in.seconds.from_now }
      cookies['scope'] = { :value => token_set['scope'], :expires => expire_in.seconds.from_now }
      refresh_token = token_set['refresh_token']

      onenote_client.save_refresh_token(refresh_token) if refresh_token.present?
    end
  end
  
end
