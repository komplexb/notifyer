require 'one_note_sharer' # see lib/one_note_sharer.rb
require 'pushbullet_api' # see lib/pushbullet.rb

include ApplicationHelper

class NotesController < ApplicationController
  before_action :require_onenote_login
  
  def index
    @oneNoteWeb_url = nil
    @response_code = nil
    @note = nil
    @has_pb_access_token = !pb_access_token.blank?
    @has_access_token = !access_token.blank?
    @onenote_revoke_url = ONENOTE_CONFIG['revoke']
    @pushbullet_revoke_url = PUSHBULLET_CONFIG['revoke']

    
    onenote_client = OneNoteSharer.new
    pushbullet = PushbulletAPI.new
    is_pushbullet_req = false;

    result = onenote_client.get_pages_in_section(access_token)
    @notes = result
    
#    rescue Exception => e
#      if !is_pushbullet_req
#        @response = e.response
#        @response_code = e.http_code
#        @response_headers = @response.headers
#      end
    
  end
  
  private 
  
  def require_onenote_login
    unless !access_token.blank?
      flash[:error] = "OneNote must be logged in to view notes. Please login below."
      redirect_to root_path # halts request cycle
    end
  end
end
