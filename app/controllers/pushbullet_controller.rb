require 'one_note_sharer' # see lib/one_note_sharer.rb
require 'pushbullet_api' # see lib/pushbullet.rb

include ApplicationHelper

class PushbulletController < ApplicationController
  def index
    pushbullet = PushbulletAPI.new
    token_set = pushbullet.handle_callback_request(params)
    if token_set.present?
#      expire_in = "Fri, 31 Dec 9999 23:59:59 GMT"
      expire_in = 604800.seconds.from_now # the token doesn't expire lets set the cookie a week long at a time
      cookies['pb_access_token'] = { :value => token_set['access_token'], :expires => expire_in }
    end
#    debugger
    redirect_to root_path
  end

  def push_note
    pushbullet = PushbulletAPI.new
    result = pushbullet.push_page pb_access_token, params[:title], params[:body], params[:url]  # correct note body format was assigned to params 
    redirect_to notes_path
  end

  def push_random_note
    onenote_client = OneNoteSharer.new
    access_token = cookies['access_token']
    notes = onenote_client.get_pages_in_section(access_token)
    
    prng = Random.new
    rand_index = prng.rand(notes.length)  # get me a random number between 0 and total notes
    note = notes[rand_index]  # use that number to select a note
	pushbullet = PushbulletAPI.new
    pb = pushbullet.push_page pb_access_token, note[:title], note[:body][:text], note[:url] # note we have to specify the note body format
    redirect_to notes_path
  end
end
