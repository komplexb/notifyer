require "_payload"
require "logger"
require 'nokogiri'
require 'open-uri'

#$LOG = Logger.new('RestClient.log')

class OneNoteSharer
  CLIENTID = ONENOTE_CONFIG['clientId']
  CALLBACK = ONENOTE_CONFIG['callback']
  CLIENTSECRET = ONENOTE_CONFIG['secret']
  SECTION_TTILE = ONENOTE_CONFIG['section_title']

  OAUTH_AUTHORIZE_URL = "https://login.live.com/oauth20_authorize.srf"
  OAUTH_TOKEN_URL = "https://login.live.com/oauth20_token.srf"
  SHARE_URL = 'https://www.onenote.com/api/v1.0/pages'
  SECTION_URL = 'https://www.onenote.com/api/v1.0/sections/'

  def auth_url
    #    scopes = ["wl.signin", "wl.basic", "wl.offline_access", "office.onenote_create"]
    scopes = ["wl.signin", "wl.basic", "wl.offline_access", "office.onenote"] # https://msdn.microsoft.com/EN-US/library/office/dn807159.aspx
    redirect_url = "#{ONENOTE_CONFIG['callback']}&response_type=code&scope=#{scopes.join(' ')}"

    "#{OAUTH_AUTHORIZE_URL}?client_id=#{CLIENTID}&display=page&locale=en&redirect_uri=#{URI.escape(redirect_url)}"
  end

  def request_access_token (content)
    payload = {
        :client_id => CLIENTID,
        :redirect_uri => CALLBACK,
        :client_secret => CLIENTSECRET
    }
    response = RestClient.post(OAUTH_TOKEN_URL, payload.merge(content))
    JSON.parse(response)
  rescue Exception => exception
    nil
  end

  def request_access_token_by_verifier (verifier)
    request_access_token ({
        :code => verifier,
        :grant_type => 'authorization_code'
    })
  end

  def request_access_token_by_refresh (refresh_token)
    request_access_token ({
        :refresh_token => refresh_token,
        :grant_type => 'refresh_token'
    })
  end
  
  def read_refresh_token
    # read refresh token of the user identified by the site.
    nil
  end

  def save_refresh_token (refresh_token)
    # save the refresh token and associate it with the user identified by your site credential system.
  end

  def handle_callback_request (params)
    if params['access_token'].present?
      return nil
    end

    verifier = params['code']
    if verifier.present?
      token_set = request_access_token_by_verifier(verifier)
      save_refresh_token(token_set['refresh_token'])
      return token_set
    end

    refresh_token = read_refresh_token
    if refresh_token.present?
      token_set = request_access_token_by_refresh(refresh_token)
      save_refresh_token(token_set['refresh_token'])
      return token_set
    end

    nil
  end

  def get_post_headers (access_token, type=nil)
    return false if access_token.blank?
    #Since cookies are user-supplied content, it must be encoded to avoid header injection
    encoded_access_token = URI::escape(access_token)
    if type=='multipart'
      headers = {:content_type => "multipart/form-data; boundary=#{@boundary}", :'Authorization' => "Bearer #{encoded_access_token}"}
    else
      headers = {:content_type => "text/html", :'Authorization' => "Bearer #{encoded_access_token}"}
    end

    headers
  end

  def get_pages_in_section access_token
    encoded_access_token = URI::escape(access_token)
    params = {:params => {:select => 'title,contentUrl,links'}}
    headers = {:content_type => "application/json", :Authorization => "Bearer #{encoded_access_token}", :params => {:select => 'title,contentUrl,links'}}
    page_url = get_pages_url(encoded_access_token)
    response = RestClient.get(page_url, headers){|response, request, result| response }
    new_notes = notes_hash access_token, JSON.parse(response)  # build new hash from the response
    
    return new_notes
  end

  private

  def get_pages_url encoded_access_token
    section_headers = {:content_type => "application/json", :Authorization => "Bearer #{encoded_access_token}", :params => {:filter => "name eq '#{SECTION_TTILE}'", :select => "pagesUrl"}}
    section_response = RestClient.get(SECTION_URL, section_headers){|response, request, result| response }
    pages_url = JSON.parse(section_response)["value"][0]["pagesUrl"]
    
    return pages_url
  end

  def notes_hash access_token, notes
    new_notes = []
    notes["value"].each do |note|
      note_body = get_page_content access_token, note["contentUrl"] # we only get a note URL so we have to go to it to extract the actual note
      url = format_app_url note["links"]["oneNoteClientUrl"]["href"]  # make app url android friendly
      new_notes.push({:title => note["title"], :body => note_body, :url => url})  # an array of hashes with prepped values
    end
    return new_notes
  end

  def get_page_content access_token, contentUrl
    headers = get_post_headers(access_token)
    response = RestClient.get URI::escape(contentUrl), headers
    
    doc = Nokogiri::HTML(response).css('*').remove_attr('style')
#    debugger
    doc_html = doc.css("body div").inner_html.html_safe()
    #    doc_text = doc.css("body div").remove_attr("style").inner_text # need to strip tabs etc
    
    doc_text = ''
    doc.css("body div > p").each do |item|
      doc_text.concat(item.text).concat("\n") # cleaner than inner_text
    end
    
    note_types = {:html => doc_html, :text => doc_text}
#    debugger
    return note_types
  end

  # On Android devices, you need to enclose GUIDs in the oneNoteClientUrl with braces
  def format_app_url url
    app_url = url.gsub(/([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})/, '{\1}')
    return URI::escape(app_url) # lets escape unsafe characters while we're at it
  end
end

