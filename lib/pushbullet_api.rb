require "_payload"
require "json"

class PushbulletAPI
  AUTH_BEARER = "v1mBT4y92Gij3x6YtvVbl13rcwUemc9d0dujwUz7NTrCm"
  ENDPOINT = "https://api.pushbullet.com/v2/pushes"
  ENABLE_LINKS = false
  
  CLIENTID = PUSHBULLET_CONFIG['clientId']
  CALLBACK = PUSHBULLET_CONFIG['callback']
  CLIENTSECRET = PUSHBULLET_CONFIG['secret']
  
  OAUTH_TEST_URL = "https://www.pushbullet.com/authorize?client_id=CjhcmlAnVl3dd2TCecBCY94cJaReYClv&redirect_uri=notifyer.dev%2F&response_type=code&scope=everything"
  OAUTH_AUTHORIZE_URL = "https://www.pushbullet.com/authorize"
  OAUTH_TOKEN_URL = "https://api.pushbullet.com/oauth2/token"

  def auth_url
    redirect_url = "#{PUSHBULLET_CONFIG['callback']}&response_type=code&scope=everything"

    return "#{OAUTH_AUTHORIZE_URL}?client_id=#{CLIENTID}&redirect_uri=#{URI.escape(redirect_url)}"
  end

  def request_access_token (content)
    payload = {
        :client_id => CLIENTID,
        :redirect_uri => CALLBACK,
        :client_secret => CLIENTSECRET
    }
    response = RestClient.post(OAUTH_TOKEN_URL, payload.merge(content))
    JSON.parse(response)
  rescue Exception => exception
    nil
  end

  def request_access_token_by_verifier (verifier)
    request_access_token ({
        :code => verifier,
        :grant_type => 'authorization_code'
    })
  end

  def request_access_token_by_refresh (refresh_token)
    request_access_token ({
        :refresh_token => refresh_token,
        :grant_type => 'refresh_token'
    })
  end
  
  def read_refresh_token
    # read refresh token of the user identified by the site.
    nil
  end

  def save_refresh_token (refresh_token)
    # save the refresh token and associate it with the user identified by your site credential system.
  end

  def handle_callback_request (params)
    if params['access_token'].present?
      return nil
    end

    verifier = params['code']
    if verifier.present?
      token_set = request_access_token_by_verifier(verifier)
#      save_refresh_token(token_set['refresh_token'])
      return token_set
    end

    refresh_token = read_refresh_token
    if refresh_token.present?
      token_set = request_access_token_by_refresh(refresh_token)
#      save_refresh_token(token_set['refresh_token'])
      return token_set
    end

    nil
  end
  

  def push_page(access_token, title, body, url='')
#    debugger
    return false if access_token.blank?
    #Since cookies are user-supplied content, it must be encoded to avoid header injection
    encoded_access_token = URI::escape(access_token)
    
    post = {:type => "note", :title => title, :body => body}  # write post as a hash then convert to json
#    debugger
    # conditionally push note with link
    if !url.empty? && ENABLE_LINKS
      post.merge!({:type => "link", :url => url})
    end
    
    # push note
    response = RestClient.post ENDPOINT, post.to_json, {:content_type => :json, :accept => :json, :Authorization => "Bearer #{encoded_access_token}"}
    
    JSON.parse(response)
  end

end