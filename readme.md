# Preface #
Having worked through [*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/), I set out to apply my new knowledge by building a bespoke application.


# Introduction #
Notifyer is a proof of concept solution to a problem I've been thinking about for a long time. Namely:

* How do I get a daily quote from my curated collection?
* How can I effortlessly resurface a buried quote so I can re-apply it's inspiration and lessons to the present?

Using Microsoft's OneNote as a datastore, Notifyer utilizes OneNote's API to retrieve notes from a given section. A note is then selected to be pushed to your devices via Pushbullet's API.

The ideal use case is to schedule Notifyer's random method to be invoked periodically. In fact Notifyer's functionality would work great bundled as a [Zapier App](https://bitbucket.org/repo/88y78M/images/1044131580-Screen%20Shot%202015-07-29%20at%2011.25.37%20AM.png), however for the purposes of this demo, users can select a note to be pushed immediately.  

# How? #
Microsoft helpfully provides a Github repo of [OneNote API code samples](https://github.com/OneNoteDev). The [Rails sample](https://github.com/OneNoteDev/OneNoteAPISampleRuby#api-functionality-demonstrated-in-this-sample) while useful, only demos methods of creating note pages, but not note page retrieval. This meant that I had to figure out how to build my desired functionality. Using the sample project as boilerplate, I set out to accomplish the following:  

* Find OneNote section with given section name.
* Retrieve all pages within that section
* Retrieve and parse the HTML content of a given page with nokogiri
* Post parsed page content to Pushbullet  

# Reference/Resources #
* OneNote Dev Center: http://dev.onenote.com/
* Test query strings and see what your responses should look like: https://apigee.com/onenote/embed/console/onenote/
* Pushbullet API Documentation: https://docs.pushbullet.com/
* Ruby REST Client: https://github.com/rest-client/rest-client

# Next Steps: Scheduling #
[Zapier](www.zapier.com) has a Developer Platform that looks as if it could provide scheduling once I expose a couple API endpoints. Ultimately the end goal is the mockup below.
![Screen Shot 2015-07-29 at 11.25.37 AM.png](https://bitbucket.org/repo/88y78M/images/1044131580-Screen%20Shot%202015-07-29%20at%2011.25.37%20AM.png)

  
* * *
### P.S. ###
Notifyer serves as my "learn by doing" vehicle as I get familiar with Rails, so there may be some inelegance and rookie mistakes.