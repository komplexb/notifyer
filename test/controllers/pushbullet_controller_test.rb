require 'test_helper'

class PushbulletControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get push_note" do
    get :push_note
    assert_response :success
  end

  test "should get push_random_note" do
    get :push_random_note
    assert_response :success
  end

end
