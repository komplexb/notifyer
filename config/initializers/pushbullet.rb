require 'yaml'

PUSHBULLET_CONFIG = YAML.load_file("config/pushbullet.yml")[Rails.env]
