# -*- encoding: utf-8 -*-
# stub: rest-client-multipart 1.1.0 ruby lib

Gem::Specification.new do |s|
  s.name = "rest-client-multipart"
  s.version = "1.1.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Adam Wiggins", "Archiloque"]
  s.date = "2009-12-27"
  s.description = "A simple REST client for Ruby, inspired by the Sinatra microframework style of specifying actions: get, put, post, delete."
  s.email = "rest.client@librelist.com"
  s.executables = ["restclient"]
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["README.rdoc", "bin/restclient"]
  s.homepage = "http://github.com/archiloque/rest-client"
  s.rdoc_options = ["--charset=UTF-8"]
  s.rubyforge_project = "rest-client-multipart"
  s.rubygems_version = "2.4.8"
  s.summary = "Simple REST client for Ruby, inspired by microframework syntax for specifying actions."

  s.installed_by_version = "2.4.8" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<mime-types>, [">= 1.16"])
    else
      s.add_dependency(%q<mime-types>, [">= 1.16"])
    end
  else
    s.add_dependency(%q<mime-types>, [">= 1.16"])
  end
end
